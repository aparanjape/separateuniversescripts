#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo 'Separate Universe Sims'	
  echo "USAGE: $0 <SUB_CONFIG_FILE>"
  echo " e.g.: $0 sub_suWMAP7.conf"
  exit 1
fi

# This script should be run from the current folder, which should also have
# the subfolder 'config/' containing the following files:
# -- outputs.txt : scale factors for snapshots in fiducial simulation
# -- CAMB transfer function for fiducial cosmology
# -- ics_su_template.conf     (template config file for MUSIC)
# -- run.param.su.template    (template config file for GADGET2)
# -- rockstar_su_template.cfg (template config file for ROCKSTAR)
# -- submission config file in standard format
#
# Config files will be written to the folder 'config/'
# Two additional files 'outputs.txt' and 'deltacustom.txt' will be written to
# a folder whose path is determined by the value of SCRATCH below.

##########################
# Specify output directory
##########################
# Change SCRATCH to desired top level output directory with write access, e.g. /scratch/aseem .
# The script will use this folder name in generating the config files, and also in writing
# out additional files with output snapshots and mass definitions in the current cosmology.
SCRATCH=$PWD/output 

# general setup
SCF=$PWD/config/$1
SIM_NAME=`awk '$1=="SIM_NAME" {print $3}' $SCF`
SIM_NPART=`awk '$1=="SIM_NPART" {print $3}' $SCF`
SIM_DELTA=`awk '$1=="SIM_DELTA" {print $3}' $SCF`
SIM_REAL=`awk '$1=="SIM_REAL" {print $3}' $SCF`
SIM_SEED=`awk '$1=="SIM_SEED" {print $3}' $SCF`
CAMB_TRANSFER=`awk '$1=="CAMB_TRANSFER" {print $3}' $SCF`
CAMB_TRANSFER_FID=$PWD/config/$CAMB_TRANSFER
CAMB_TRANSFER=${CAMB_TRANSFER_FID::${#CAMB_TRANSFER_FID}-4}\_SUdelta$SIM_DELTA.txt

ROCKSTAR_STUB=$SIM_NAME$SIM_NPART
SIM_STUB=$ROCKSTAR_STUB.delta$SIM_DELTA

# Fiducial simulation
LBOX_FID=`awk '$1=="LBOX_FID" {print $3}' $SCF`
A_START_FID=`awk '$1=="A_START_FID" {print $3}' $SCF`
A_FIN_FID=`awk '$1=="A_FIN_FID" {print $3}' $SCF`
OUTPUTS_FID=`awk '$1=="OUTPUTS_FID" {print $3}' $SCF`

# Fiducial cosmology
OMEGA_M_FID=`awk '$1=="OMEGA_M_FID" {print $3}' $SCF`
OMEGA_L_FID=`awk '$1=="OMEGA_L_FID" {print $3}' $SCF`
OMEGA_B_FID=`awk '$1=="OMEGA_B_FID" {print $3}' $SCF`
HUBBLE_FID=`awk '$1=="HUBBLE_FID" {print $3}' $SCF`
SIGMA8_FID=`awk '$1=="SIGMA8_FID" {print $3}' $SCF`
NS_FID=`awk '$1=="NS_FID" {print $3}' $SCF`

NCORE=8
LGNPART=6
CUBE=256
NFILE=1
NWRITER=16
case $SIM_NPART in
  128) 
    NCORE=16; LGNPART=7; CUBE=128; NWRITER=8
    ;;
  256) 
    NCORE=64; LGNPART=8
    ;;
  512) 
    NCORE=256; LGNPART=9
    ;;
  1024) 
    NCORE=256; LGNPART=10; NFILE=8; 
    ;;
esac

# set python executable
PYTHON_EXEC=python2.7
 
# setup MUSIC
MUSIC_OUT_DIR=$SCRATCH/$ROCKSTAR_STUB/ICs
MUSIC_OUT_FILE=$MUSIC_OUT_DIR/ics\_$ROCKSTAR_STUB\_delta$SIM_DELTA\_r$SIM_REAL.dat
MUSIC_CONFIG_FILE=$PWD/config/ics\_$ROCKSTAR_STUB\_delta$SIM_DELTA\_r$SIM_REAL.conf
MUSIC_TEMPLATE=$PWD/config/ics\_su\_template.conf

# setup Gadget2
GADGET2_OUT_DIR=$SCRATCH/$ROCKSTAR_STUB/sims/delta$SIM_DELTA/r$SIM_REAL
GADGET2_CONFIG_FILE=$PWD/config/run.param.$SIM_STUB.r$SIM_REAL
GADGET2_TEMPLATE=$PWD/config/run.param.su.template

# setup Rockstar
ROCKSTAR_CONFIG_FILE=$PWD/config/rockstar\_$ROCKSTAR_STUB\_delta$SIM_DELTA\_r$SIM_REAL.cfg
AUTO_ROCKSTAR_DIR=$SCRATCH/$ROCKSTAR_STUB/halos/delta$SIM_DELTA/r$SIM_REAL
ROCKSTAR_TEMPLATE=$PWD/config/rockstar\_su\_template.cfg

#################
echo 'checking that output directories exist'
if [ ! -d $MUSIC_OUT_DIR ]; then
  echo "making directory: $MUSIC_OUT_DIR"
  mkdir -p $MUSIC_OUT_DIR
fi
if [ ! -d $GADGET2_OUT_DIR ]; then
  echo "making directory: $GADGET2_OUT_DIR"
  mkdir -p $GADGET2_OUT_DIR
fi
if [ ! -d $AUTO_ROCKSTAR_DIR ]; then
  echo "making directory: $AUTO_ROCKSTAR_DIR"
  mkdir -p $AUTO_ROCKSTAR_DIR
fi

# Calculate and write out new cosmology
COSMO_TXT=$PWD/config/cosmo.$SIM_STUB.txt
OUTPUTS_TXT=$SCRATCH/$ROCKSTAR_STUB/sims/delta$SIM_DELTA/outputs.txt
DELTA_CUSTOM_TXT=$SCRATCH/$ROCKSTAR_STUB/sims/delta$SIM_DELTA/deltacustom.txt
echo 'calculating new cosmology...'
$PYTHON_EXEC $PWD/util/calc_cosmo_SU.py $OMEGA_M_FID $OMEGA_L_FID $OMEGA_B_FID\
	   				$HUBBLE_FID $SIGMA8_FID $NS_FID\
				        $LBOX_FID $A_START_FID $A_FIN_FID\
					$OUTPUTS_FID $CAMB_TRANSFER_FID $CAMB_TRANSFER\
					$SIM_DELTA $SIM_NPART \
					$COSMO_TXT $OUTPUTS_TXT $DELTA_CUSTOM_TXT

OMEGA_M=`awk 'NR==1 {print $1}' $COSMO_TXT`
OMEGA_L=`awk 'NR==2 {print $1}' $COSMO_TXT`
HUBBLE=`awk 'NR==3 {print $1}' $COSMO_TXT`
H0=`awk 'NR==4 {print $1}' $COSMO_TXT`
SIGMA8=`awk 'NR==5 {print $1}' $COSMO_TXT`
LBOX=`awk 'NR==6 {print $1}' $COSMO_TXT`
A_START=`awk 'NR==7 {print $1}' $COSMO_TXT`
A_FIN=`awk 'NR==8 {print $1}' $COSMO_TXT`
Z_START=`awk 'NR==9 {print $1}' $COSMO_TXT`
FORCE_RES=`awk 'NR==10 {print $1}' $COSMO_TXT`
DELTA_CUSTOM=`awk 'NR==11 {print $1}' $COSMO_TXT`

N_OUT=`awk 'END {print FNR}' $OUTPUTS_TXT`
N_OUT=$(expr $N_OUT + 1)

echo 'writing config files'
echo '... music'
cp $MUSIC_TEMPLATE $MUSIC_CONFIG_FILE
sed -i -e "s#^filename.*#filename		= $MUSIC_OUT_FILE#" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^gadget_num_files.*/gadget_num_files	= $NFILE/" "$MUSIC_CONFIG_FILE"

sed -i -e "s/^seed.*/seed[$LGNPART]		= $SIM_SEED/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^cubesize.*/cubesize	= $CUBE/" "$MUSIC_CONFIG_FILE"

sed -i -e "s/^boxlength.*/boxlength		= $LBOX/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^zstart.*/zstart			= $Z_START/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^levelmin	.*/levelmin		= $LGNPART/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^levelmin\_TF.*/levelmin\_TF		= $LGNPART/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^levelmax	.*/levelmax		= $LGNPART/" "$MUSIC_CONFIG_FILE"

sed -i -e "s/^Omega\_m.*/Omega\_m		= $OMEGA_M/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^Omega\_L.*/Omega\_L		= $OMEGA_L/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^Omega\_b.*/Omega\_b		= $OMEGA_B_FID/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^H0.*/H0		= $H0/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^sigma\_8.*/sigma\_8		= $SIGMA8/" "$MUSIC_CONFIG_FILE"
sed -i -e "s/^nspec.*/nspec		= $NS_FID/" "$MUSIC_CONFIG_FILE"
sed -i -e "s#^transfer\_file.*#transfer\_file	= $CAMB_TRANSFER#" "$MUSIC_CONFIG_FILE"

echo '... gadget2'
cp $GADGET2_TEMPLATE $GADGET2_CONFIG_FILE
sed -i -e "s#InitCondFile.*#InitCondFile	 $MUSIC_OUT_FILE#" "$GADGET2_CONFIG_FILE"
sed -i -e "s#OutputDir.*#OutputDir	 $GADGET2_OUT_DIR#" "$GADGET2_CONFIG_FILE"
sed -i -e "s#OutputListFilename.*#OutputListFilename	 $OUTPUTS_TXT#" "$GADGET2_CONFIG_FILE"

sed -i -e "s/^TimeBegin.*/TimeBegin		 $A_START/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^TimeMax.*/TimeMax		 $A_FIN/" "$GADGET2_CONFIG_FILE"

sed -i -e "s/^Omega0.*/Omega0		 $OMEGA_M/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^OmegaLambda.*/OmegaLambda	 $OMEGA_L/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^OmegaBaryon.*/OmegaBaryon	 $OMEGA_B_FID/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^HubbleParam.*/HubbleParam	 $HUBBLE/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^BoxSize.*/BoxSize		 $LBOX/" "$GADGET2_CONFIG_FILE"

sed -i -e "s/^NumFilesPerSnapshot.*/NumFilesPerSnapshot		 $NFILE/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^NumFilesWrittenInParallel.*/NumFilesWrittenInParallel	 $NFILE/" "$GADGET2_CONFIG_FILE"

sed -i -e "s/^SofteningHalo .*/SofteningHalo	 $FORCE_RES/" "$GADGET2_CONFIG_FILE"
sed -i -e "s/^SofteningHaloMaxPhys.*/SofteningHaloMaxPhys	 $FORCE_RES/" "$GADGET2_CONFIG_FILE"

echo '... rockstar'
cp $ROCKSTAR_TEMPLATE $ROCKSTAR_CONFIG_FILE
sed -i -e "s#^INBASE.*#INBASE = \"$GADGET2_OUT_DIR\"#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^FORCE\_RES.*#FORCE\_RES = $FORCE_RES#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^NUM\_SNAPS.*#NUM\_SNAPS = $N_OUT#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^NUM\_BLOCKS.*#NUM\_BLOCKS = $NFILE#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^NUM\_WRITERS.*#NUM\_WRITERS = $NWRITER#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^FORK\_PROCESSORS\_PER\_MACHINE.*#FORK\_PROCESSORS\_PER\_MACHINE = $NWRITER#" "$ROCKSTAR_CONFIG_FILE"
sed -i -e "s#^MASS_DEFINITION5.*#MASS_DEFINITION5 = \"$DELTA_CUSTOM\b\"#" "$ROCKSTAR_CONFIG_FILE"
if [ "$NFILE" -ne 1 ]; then
    sed -i -e "s#^FILENAME.*#FILENAME = \"snapshot_<snap>.<block>\"#" "$ROCKSTAR_CONFIG_FILE"
fi

echo "... all done!"
#################

