# README #

Scripts for generating configuration files to run N-body simulations using the Separate Universe technique. Currently supported formats include:

* Initial conditions: -- [MUSIC](https://bitbucket.org/ohahn/music) 
* N-body code       : -- [GADGET-2](https://wwwmpa.mpa-garching.mpg.de/gadget/)
* Halo finder       : -- [ROCKSTAR](https://bitbucket.org/gfcstanford/rockstar)

Requires Python2.7 and [NumPy](https://pypi.python.org/pypi/numpy).

## Usage: ##
The shell script ***writeconfig\_SepUniv.sh*** takes as an argument the name of a user-specified  configuration file

```
#!bash

./writeconfig_SepUniv.sh <SUBMISSION_CONFIG_FILE>
```

and calls a Python script ***calc\_cosmo\_SU.py*** to perform calculations necessary for the Separate Universe setup. It then writes out configuration files for MUSIC, GADGET-2 and ROCKSTAR.

The directory structure in this repository should be maintained: the directory containing the shell script should also have subdirectories **util/** containing the Python script and **config/** containing the following files:

* submission config file (sample **sub_suWMAP7.conf** provided)
* **outputs.txt** : scale factors for snapshots in fiducial simulation
* [CAMB](http://camb.info/) transfer function for fiducial cosmology
* **ics_su_template.conf**     (template config file for MUSIC)
* **run.param.su.template**    (template config file for GADGET2)
* **rockstar_su_template.cfg** (template config file for ROCKSTAR)


The template files are used by the shell script to create and manipulate the final configuration files, which are written to **config/**. Two additional files **outputs.txt** and **deltacustom.txt** will be written to a directory whose path is determined by the variable SCRATCH defined in ***writeconfig\_SepUniv.sh***. SCRATCH also controls various output path names written in the configuration files. Change SCRATCH to the desired top level simulation output directory with write access, e.g. /scratch/aseem. 

## Contact: ##
Aseem Paranjape 

## Literature: ##
* Paranjape & Padmanabhan (2016), https://arxiv.org/abs/1612.02833
* Lazeyras, Wagner, Baldauf & Schmidt (2016), https://arxiv.org/abs/1511.01096
* Wagner, Schmidt, Chiang & Komatsu (2015), https://arxiv.org/abs/1409.6294