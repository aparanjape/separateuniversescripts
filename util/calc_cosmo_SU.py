#!/usr/bin/python

import sys
import numpy as ny

if(len(sys.argv)==18):
    Om_fid = float(sys.argv[1])
    Ol_fid = float(sys.argv[2])
    Ob_fid = float(sys.argv[3])
    h_fid = float(sys.argv[4])
    sigma8_fid = float(sys.argv[5])
    ns_fid = float(sys.argv[6])
    Lbox_fid = float(sys.argv[7])
    astart_fid = float(sys.argv[8])
    afin_fid = float(sys.argv[9])
    outputs_fid = sys.argv[10]
    tf_fid = sys.argv[11]
    tf_file = sys.argv[12]
    delta = float(sys.argv[13])
    npart = int(sys.argv[14])
    out_cosmo = sys.argv[15]
    out_outputs = sys.argv[16]
    out_Delta = sys.argv[17]
else: 
    Om_fid = float(raw_input("Specify fiducial Om: "))
    Ol_fid = float(raw_input("Specify fiducial Ol: "))
    Ob_fid = float(raw_input("Specify fiducial Ob: "))
    h_fid = float(raw_input("Specify fiducial h: "))
    sigma8_fid = float(raw_input("Specify fiducial sigma8: "))
    ns_fid = float(raw_input("Specify fiducial ns: "))
    Lbox_fid = float(raw_input("Specify fiducial Lbox (Mpc/h): "))
    astart_fid = float(raw_input("Specify starting scale factor in fiducial: "))
    afin_fid = float(raw_input("Specify ending scale factor in fiducial: "))
    outputs_fid = raw_input("Specify fiducial file for output scale factors: ")
    tf_fid = raw_input("Specify fiducial transfer func file: ")
    tf_file = raw_input("Specify output transfer func file: ")
    delta = float(raw_input("Specify deltaL0: "))
    npart = int(raw_input("Specify n_part (e.g. 128): "))
    out_cosmo = raw_input("Specify output file for cosmology: ")
    out_outputs = raw_input("Specify output file for output scale factors: ")
    out_Delta = raw_input("Specify output file for custom Delta values: ")

ngrid=7000
ngrid_large=21000
aMIN = 1e-10
NOTSOTINY = 1.0e-6

@ny.vectorize
def Wth(x):
    """ TopHat filter W(x) = (3/x) j1(x) """
    xsq = x**2
    if ny.fabs(x) < NOTSOTINY:
        xq = xsq**2
        return 1.0 - xsq/10.0 + xq/280.0 - xsq*xq/15120.0
    else: return (3.0/x/xsq)*(ny.sin(x)-x*ny.cos(x))

def E(a,Om,Ol):
   return ny.sqrt(Om/a**3+Ol+(1-Om-Ol)/a**2)

def Eint(a,Om,Ol):
   return 2.5*Om / (a**3 * E(a,Om,Ol)**3)

def GrowthFactor(a,Om,Ol):
   atab = ny.linspace(aMIN,a,ngrid_large)
   da = atab[-1]-atab[-2]
   D = E(a,Om,Ol)*ny.trapz(Eint(atab,Om,Ol),dx=da);
   return D

def age(a,Om,Ol):
   atab = ny.linspace(aMIN,a,ngrid_large)
   da = atab[-1]-atab[-2]
   T = ny.trapz(1.0/(atab*E(atab,Om,Ol)),dx=da);
   # dimensionless age: T = H0 * t
   return T

D_fid = GrowthFactor(1,Om_fid,Ol_fid)

KbyH0sq = 5*Om_fid*delta/(3.0*D_fid)
if KbyH0sq > 1.0: sys.exit("invalid parameters!")

deltaH_plus1 = ny.sqrt(1.0-KbyH0sq)

h = h_fid*deltaH_plus1
Om = Om_fid/deltaH_plus1**2
Ol = Ol_fid/deltaH_plus1**2
Lbox = Lbox_fid*deltaH_plus1

D_new = GrowthFactor(1,Om,Ol)

aout_fid = ny.loadtxt(outputs_fid)
aout_fid = ny.append(aout_fid,afin_fid)
aout_fid = ny.insert(aout_fid,0,astart_fid)

tout = ny.zeros(len(aout_fid),dtype=float)
for i in range(len(aout_fid)):
    tout[i] = age(aout_fid[i],Om_fid,Ol_fid)

agrid = 10**ny.linspace(ny.log10(astart_fid)-1.5,ny.log10(afin_fid)+1.5,ngrid)
tgrid = ny.zeros(ngrid,dtype=float)
for J in range(ngrid):
   tgrid[J] = age(agrid[J],Om,Ol)

# Note deltaH_plus1 below. This is because dimensionless age depends on H0.
aout = 10**ny.interp(ny.log10(tout*deltaH_plus1),ny.log10(tgrid),ny.log10(agrid))
zstart = 1.0/aout[0]-1.0
force_res = Lbox/npart/30.0

Delta_custom = 200.0*(aout/aout_fid)**3

transfer = ny.loadtxt(tf_fid)
# expect k (h/Mpc) | Tcdm | Tbary | ...
ktab = 1.0*transfer[:,0]
fbary = Ob_fid/(Om_fid-Ob_fid)
Tftab = (1.0-fbary)*transfer[:,1] + fbary*transfer[:,2]
dlnk = ny.log(ktab[1]/ktab[0])
Ghhtilde = (ny.trapz(ktab**(ns_fid+3.0)*Tftab**2*Wth(8*ktab/deltaH_plus1)**2,dx=dlnk)
	    /ny.trapz(ktab**(ns_fid+3.0)*Tftab**2*Wth(8*ktab)**2,dx=dlnk))

sigma8 = sigma8_fid*(D_new/D_fid)*ny.sqrt(Ghhtilde) 

transfer[:,0] /= deltaH_plus1

ny.savetxt(out_cosmo,ny.array([Om,Ol,h,100*h,sigma8,Lbox,aout[0],aout[-1],zstart,force_res,Delta_custom[-1]]).T,fmt='%.8f')
ny.savetxt(out_outputs,aout[1:-1].T,fmt='%.8f')
ny.savetxt(out_Delta,Delta_custom[1:].T,fmt='%.8f')
ny.savetxt(tf_file,transfer,fmt='%.5e',delimiter='  ')
